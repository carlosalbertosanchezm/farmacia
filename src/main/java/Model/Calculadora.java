/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import static java.lang.Math.round;

/**
 *
 * @author LENOVO
 */
public class Calculadora {

    double num1;
    double num2;

    public Calculadora(double num1, double num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public double getNum1() {
        return num1;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    public double getNum2() {
        return num2;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }

    public String suma() {
        return "" + (num1 + num2);

    }

    public String resta() {
        return "" + (num1 - num2);

    }

    public String multi() {
        return "" + (num1 * num2);

    }

    public String divi() {
        if (num2 == 0) {
            return "no es válido dividir por cero";
        }
        return "" + miRounder((num1 / num2), 2);

    }

    public String potencia() {
        return "" + miRounder(Math.pow(num1, num2), 2);
    }

    public String raizX() {
        if (num1 < 0) 
        {
            return "no calculo raiz negativa";
        }
        
        double aux=(1/num2);
        System.out.println("num 1="+num1+" num2="+num2+" float="+aux+" math.pow=");
        return "" + miRounder(   (Math.pow(num1,   aux )  ), 2);

      //  return "" + miRounder(Math.sqrt(num1), 2);
    }

    public String errorOpc() {
        return ".. seleccione operación";

    }

    public double miRounder(double numberRound, int decimales) {
        double redondeado = numberRound;
        switch (decimales) {
            case 1:
                redondeado = Math.round(redondeado * 10.0) / 10.0;
                break;
            case 2:
                redondeado = Math.round(redondeado * 100.0) / 100.0;
                break;
            case 3:
                redondeado = Math.round(redondeado * 1000.0) / 1000.0;
                break;
            case 4:
                redondeado = Math.round(redondeado * 10000.0) / 10000.0;
                break;
            default:
                redondeado = numberRound;
        }
        return redondeado;
    }

}
