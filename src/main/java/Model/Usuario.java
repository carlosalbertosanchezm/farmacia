/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Controller.ConnectionDB;

/**
 *
 * @author fido
 */
public class Usuario {

    private String idUsuario;
    private String nombreUsuario;
    private String correoUsuario;
    private String claveUsuario;
    private String nombrePreferido;

    private Object txtInfoUsuario;

    public Usuario() {
    }

    public Usuario(String nombreUsuario, String correoUsuario, String claveUsuario, String nombrePreferido) {
        this.nombreUsuario = nombreUsuario;
        this.correoUsuario = correoUsuario;
        this.claveUsuario = claveUsuario;
        this.nombrePreferido = nombrePreferido;
    }

    public Usuario(String idUsuario, String nombreUsuario, String correoUsuario, String claveUsuario, String nombrePreferido) {
        this.idUsuario = idUsuario;
        this.nombreUsuario = nombreUsuario;
        this.correoUsuario = correoUsuario;
        this.claveUsuario = claveUsuario;
        this.nombrePreferido = nombrePreferido;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(String claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    public String getNombrePreferido() {
        return nombrePreferido;
    }

    public void setNombrePreferido(String nombrePreferido) {
        this.nombrePreferido = nombrePreferido;
    }

    @Override
    public String toString() {
        return "Usuario{" + "idUsuario=" + idUsuario + ", nombreUsuario=" + nombreUsuario + ", correoUsuario=" + correoUsuario + ", claveUsuario=" + claveUsuario + ", nombrePreferido=" + nombrePreferido + '}';
    }

    public List<Usuario> listarUsuarios() {
        List<Usuario> listaUsuarios = new ArrayList<>();
        ConnectionDB conexion = new ConnectionDB();
        String sql = "SELECT * FROM Usuarios;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            Usuario u;
            while (rs.next()) {
                u = new Usuario();
                u.setIdUsuario(rs.getString("IdUsuario"));
                u.setNombreUsuario(rs.getString("NombreUsuario"));
                u.setNombrePreferido(rs.getString("NombrePreferido"));
                u.setCorreoUsuario(rs.getString("CorreoUsuario"));
                u.setClaveUsuario(rs.getString("ClaveUsuario"));
                listaUsuarios.add(u);
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar Usuarios:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaUsuarios;
    }

    public boolean guardarUsuario() {
        ConnectionDB conexion = new ConnectionDB();

        String sql = "INSERT INTO Usuarios(IdUsuario,NombreUsuario,NombrePreferido,CorreoUsuario,ClaveUsuario)"
                + "VALUES('" + this.idUsuario + "','" + this.nombreUsuario + "','" + this.nombrePreferido + "','" + this.correoUsuario + "','" + this.claveUsuario + "');";
        System.out.println("estoy en Guardar Usuario en Usuario.java linea 122 " + sql);

        if (conexion.setAutoCommitBD(false)) {//Para que la bd no confirme automaticamente el cambio
            if (conexion.insertarBD(sql)) {
                conexion.commitBD();//confirma el cambio a la BD
                conexion.cerrarConexion();
                System.out.println("estoy en Guardar Usuario en Usuario.java linea 128   todo Bien !!   return True " + sql);
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                System.out.println("estoy en Guardar Usuario en Usuario.java linea 133  todo Mal !!   return false  " + sql);
                return false;
            }
        } else {
            conexion.cerrarConexion();
            System.out.println("estoy en Guardar Usuario en Usuario.java linea 138 return false conexion cerrada " + sql);

            return false;
        }
    }

    public boolean actualizarUsuario() {
        ConnectionDB conexion = new ConnectionDB();

        String sql = "UPDATE Usuarios SET NombreUsuario='"
                + this.nombreUsuario + "', Direccion='" + this.nombrePreferido + " ', CorreoUsuario='" + this.correoUsuario + "', ClaveUsuario='" + this.claveUsuario + "' WHERE IdUsuario='" + this.idUsuario + "';";

        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean eliminarUsuario() {
        ConnectionDB conexion = new ConnectionDB();
        String sql = "DELETE FROM Usuarios WHERE IdUsuario='" + this.idUsuario + "';";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

}
