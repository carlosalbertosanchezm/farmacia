/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Controller.ConnectionDB;

/**
 *
 * @author fido
 */
public class Farmacia {

    private int idFarmacia;
    private String nombreFarmacia;
    private String direccion;
    private double telefono;
    private String email;
    private double nit;
    private int numLote;
    private Object txtInfoFarmacia;

    public Farmacia() {
    }

    public Farmacia(String nombreFarmacia, String direccion, double telefono, String email, double nit, int numLote) {
        this.nombreFarmacia = nombreFarmacia;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.nit = nit;
        this.numLote = numLote;
    }

    public Farmacia(int idFarmacia, String nombreFarmacia, String direccion, double telefono, String email, double nit, int numLote) {
        this.idFarmacia = idFarmacia;
        this.nombreFarmacia = nombreFarmacia;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.nit = nit;
        this.numLote = numLote;
    }

    public int getIdFarmacia() {
        return idFarmacia;
    }

    public void setIdFarmacia(int idFarmacia) {
        this.idFarmacia = idFarmacia;
    }

    public String getNombreFarmacia() {
        return nombreFarmacia;
    }

    public void setNombreFarmacia(String nombreFarmacia) {
        this.nombreFarmacia = nombreFarmacia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getTelefono() {
        return telefono;
    }

    public void setTelefono(double telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getNit() {
        return nit;
    }

    public void setNit(double nit) {
        this.nit = nit;
    }

    public int getNumLote() {
        return numLote;
    }

    public void setNumLote(int numLote) {
        this.numLote = numLote;
    }

    @Override
    public String toString() {
        return "Farmacia{" + "idFarmacia=" + idFarmacia + ", nombreFarmacia=" + nombreFarmacia + ", direccion=" + direccion + ", telefono=" + telefono + ", email=" + email + ", nit=" + nit + ", numLote=" + numLote + '}';
    }


    public List<Farmacia> listarFarmacias() {
        List<Farmacia> listaFarmacias = new ArrayList<>();
        ConnectionDB conexion = new ConnectionDB();
        String sql = "SELECT * FROM Farmacias;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            Farmacia f;
            while (rs.next()) {
                f = new Farmacia();
                f.setIdFarmacia(rs.getInt("IdFarmacia"));
                f.setNombreFarmacia(rs.getString("NombreFarmacia"));
                f.setDireccion(rs.getString("Direccion"));
                f.setTelefono(rs.getDouble("Telefono"));
                f.setEmail(rs.getString("Email"));
                f.setNit(rs.getDouble("Nit"));
                f.setNumLote(rs.getInt("NumLote"));
                listaFarmacias.add(f);
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar Farmacias:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaFarmacias;
    }

    public boolean guardarFarmacia() {
        ConnectionDB conexion = new ConnectionDB();

        String sql = "INSERT INTO Farmacias(NombreFarmacia,Direccion,Telefono,Email,Nit,NumLote)"
                + "VALUES('" + this.nombreFarmacia + "','" + this.direccion + "'," + this.telefono + ",'" + this.email + "'," + this.nit + "," + this.numLote + ");";
        System.out.println("estoy en Guardar Farmacia en farmacia.java linea 158 " + sql);

        if (conexion.setAutoCommitBD(false)) {//Para que la bd no confirme automaticamente el cambio
            if (conexion.insertarBD(sql)) {
                conexion.commitBD();//confirma el cambio a la BD
                conexion.cerrarConexion();
                System.out.println("estoy en Guardar Farmacia en Farmacia.java linea 165   todo Bien !!   return True " + sql);
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                System.out.println("estoy en Guardar farmacia en Farmacia.java linea 170 return false  " + sql);
                return false;
            }
        } else {
            conexion.cerrarConexion();
            System.out.println("estoy en GuardarFarmacia en Farmacia.java linea 175 return false conexion cerrada " + sql);

            return false;
        }
    }

    public boolean actualizarFarmacia() {
        ConnectionDB conexion = new ConnectionDB();

        String sql = "UPDATE Farmacias SET NombreFarmacia='"
                + this.nombreFarmacia + "', Direccion='" + this.direccion + "',Telefono=" + this.telefono + ", Email='" + this.email + "', Nit=" + this.nit
                + ", NumLote=" + this.numLote + " WHERE IdFarmacia=" + this.idFarmacia + ";";

        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean eliminarFarmacia() {
        ConnectionDB conexion = new ConnectionDB();
        String sql = "DELETE FROM Farmacias WHERE IdFarmacia=" + this.idFarmacia + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

}
