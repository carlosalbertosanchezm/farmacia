/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Controller.ConnectionDB;

/**
 *
 * @author fido
 */
public class Producto {

    private int idProducto;
    private String nombre;
    private double temperatura;
    private double valorBase;
    private double costo;
    private Object txtInfo;
    public String nameUser;
    //nameUser="Silvia";
   
    public Producto() {
    }

    public Producto(int idProducto, String nombre, double temperatura, double valorBase, double costo) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
        this.costo = costo;
    }

    public Producto(String nombre, double temperatura, double valorBase, double costo) {
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
        this.costo = costo;
    }

 

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }
    
public double calcularCostoAlmacenamiento (double temperatura , double valorBase) {
    double costoAlmacenamiento;
        costoAlmacenamiento = valorBase*1.10;
    if (temperatura<21){
        costoAlmacenamiento= valorBase*1.20;
    }
    return Math.round(costoAlmacenamiento*100)/100;
}  

    @Override
    public String toString() {
        return "Producto{" + "idProducto=" + idProducto + ", nombre=" + nombre + ", temperatura=" + temperatura + ", valorBase=" + valorBase + ", costo=" + costo + '}';
    }
   



    public List<Producto> listarProductos() {
        List<Producto> listaProductos = new ArrayList<>();
        ConnectionDB conexion = new ConnectionDB();
        String sql = "SELECT * FROM Productos;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            Producto p;
            while (rs.next()) {
                p = new Producto();
                p.setIdProducto(rs.getInt("IdProducto"));
                p.setNombre(rs.getString("Nombre"));
                p.setTemperatura(rs.getDouble("Temperatura"));
                p.setValorBase(rs.getDouble("ValorBase"));
                p.setCosto(rs.getDouble("Costo"));
                listaProductos.add(p);
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
    }

    public boolean guardarProducto() {
        ConnectionDB conexion = new ConnectionDB();
        
        String sql = "INSERT INTO Productos(Nombre,Temperatura,ValorBase,Costo)"
                + "VALUES('" + this.nombre + "'," + this.temperatura + "," + this.valorBase + "," +this.costo+");";
        System.out.println("estoy en Guardar producto en Producto.java linea 120 "+sql);
             
        
        if (conexion.setAutoCommitBD(false)) {//Para que la bd no confirme automaticamente el cambio
            if (conexion.insertarBD(sql)) {
                conexion.commitBD();//confirma el cambio a la BD
                conexion.cerrarConexion();
    System.out.println("estoy en Guardar producto en Producto.java linea 127   todo Bien !!   return True "+sql);
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
     System.out.println("estoy en Guardar producto en Producto.java linea 132 return false  "+sql);
                return false;
            }
        } else {
            conexion.cerrarConexion();
     System.out.println("estoy en Guardar producto en Producto.java linea 137 return false conex cerrada "+sql);

            return false;
        }
    }

    public boolean actualizarProducto() {
        ConnectionDB conexion = new ConnectionDB();
        
        
        String sql = "UPDATE Productos SET Nombre='"
                + this.nombre + "',Temperatura=" + this.temperatura
                + ",ValorBase=" + this.valorBase + ",Costo="
                +this.costo + " WHERE IdProducto=" + this.idProducto + ";";
                
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean eliminarProducto() {
        ConnectionDB conexion = new ConnectionDB();
        String sql = "DELETE FROM Productos WHERE IdProducto=" + this.idProducto + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

}
